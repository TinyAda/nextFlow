package types

import (
	"context"
	"github.com/gammazero/workerpool"
)

type FlowStruct struct {
	Ctx        context.Context
	Wp         *workerpool.WorkerPool
	Nodes      map[string]Node
	DebugFuc   func(v ...interface{})
	TriggerFuc func(v ...interface{})
	Config     H
}
