package types

import (
	"context"
	//"encoding/json"
	// "fmt"
	// "github.com/gammazero/workerpool"
	"github.com/tidwall/gjson"
	//"github.com/tidwall/sjson"
	//"reflect"
	//"strings"
)

const (
	True  = "true"
	False = "false"
	Fail  = "fail"
)

type Node interface {
	SetFlowCtx(ctx Flow)
	Type() string
	Index() string
	Init() error
	OnHandle() error
	HandleNext(relationType string)
	Destroy()
	ConfigGet(path string, def ...interface{}) gjson.Result
	ConfigSet(path string, value interface{}) error
}

type Flow interface {
	Trace(v ...interface{})
	// SetContext(c context.Context) *interface{}
	Trigger(v ...interface{}) any
	JsDo(config H) (outFunc interface{}, outTrigger interface{}, err error)
	CetContext() context.Context
	SubmitTack(task func())
	GetNextNodes(index string, cond string) ([]Node, bool)
	ExprCalc(str string, id ...string) string
}
