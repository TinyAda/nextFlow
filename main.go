package main

import (
	//"nextFlow/action/js"
	//	"nextFlow/engine"
	"fmt"
	"github.com/dop251/goja"
	"nextFlow/flow"
	_ "nextFlow/native_modules"
	"nextFlow/node"
	"nextFlow/types"
	"nextFlow/utils"
	// "path/filepath"
	"reflect"
	"time"
)

func test() {
	start := time.Now()

	elapsed := time.Now().Sub(start)

	start = time.Now()
	var jsScript = `function Expr(){ return true==true; }`
	jsEngine := utils.NewGojaJsEngine(jsScript, "", nil)
	out, err := jsEngine.Execute("test")
	fmt.Println(reflect.TypeOf(out), err)
	elapsed = time.Now().Sub(start)
	fmt.Println("test 执行时间 goja => ", elapsed)
}

func test2() {

	jsEngine := utils.NewGojaJsEngine("", "", nil)
	entrypoint, err := jsEngine.Execute("require", "main.js")

	if err != nil {
		fmt.Println(err)
		return
	}

	mainjs, ok := entrypoint.(*goja.Object) // (map[string]interface {})

	init, ok := goja.AssertFunction(mainjs.Get("init"))

	if ok {
		init(mainjs, jsEngine.ToValue("123123123"))
	}

	fmt.Println(err)

}

/*func testgoja() {
	start := time.Now()
	out, _ := engine.Do(types.H{
		"src":  "main.js",
		"func": "sendMessage",
		"init": types.H{
			"e":  "init",
			"dd": 1,
		},
		"params": types.H{
			"text": "内容",
			"obj": types.H{
				"t":    "1",
				"fuck": "1",
			},
		},
	})
	elapsed := time.Now().Sub(start)
	fmt.Println(out.(bool), elapsed)
	utils.TransformES5("")
}*/

func main() {

	/*	utils.ReadFile("main")

		return*/

	//test2()

	// return

	/*sdf := types.H{
		"@this": `{"test":"测试"}`,
	}
	fmt.Println(sdf.Get("test"))

	return*/

	flow.New().AddNode(node.New(types.H{
		"index":   "1",
		"type":    "trigger",
		"request": types.H{},
		"inputs":  types.H{},
		"outputs": types.H{},
		"children": []types.H{
			types.H{"from": "1", "to": "2"},
		},
	})).AddNode(node.New(types.H{
		"index": "2",
		"type":  "if",
		"inputs": types.H{
			"if": types.H{
				"cond": "${nodes.2.inputs.result} == true",
			},
			"result": true,
		},
		"outputs": types.H{},
		"children": []types.H{
			types.H{"from": "2", "to": "3", "cond": "true"},
		},
	})).AddNode(node.New(types.H{
		"index": "3",
		"type":  "while",
		"inputs": types.H{
			"while": types.H{
				"current": 0,
				"max":     1,
				"cond":    "${nodes.3.inputs.while.current} < ${nodes.3.inputs.while.max}",
			},
		},
		"outputs": types.H{},
		"children": []types.H{
			types.H{"from": "3", "to": "4", "cond": "true"},
		},
	})).AddNode(node.New(types.H{
		"index": "4",
		"type":  "func",
		"inputs": types.H{
			"src":  "main",
			"func": "sendMessage",
			"init": types.H{
				"ee": "ee",
				"dd": 1,
			},
			"params": types.H{
				"text": "内容",
				"obj": types.H{
					"t":    "1",
					"fuck": "1",
				},
			},
		},
		"outputs": types.H{},
		"children": []types.H{
			types.H{"from": "4", "to": "3"},
		},
	})).Debug(func(v ...interface{}) {
		fmt.Println(v...)
	}).Start().End(func() {
		fmt.Println("结束")
	})

}
