package utils

import (
	"fmt"
	"github.com/dop251/goja"
	"nextFlow/types"
)

func Expr(str string) string {
	var jsScript = fmt.Sprintf(`function Expr(){ return %s; }`, str)
	jsEngine := NewGojaJsEngine(jsScript, "", nil)
	out, err := jsEngine.Execute("Expr")
	if err == nil {
		if out.(goja.Value).Export().(bool) {
			return types.True
		}
		return types.False
	}
	return types.Fail
}
