package utils

import (
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"nextFlow/types"
)

func GetQueryParams(c *gin.Context) map[string]any {
	query := c.Request.URL.Query()
	var queryMap = make(map[string]any, len(query))
	for k := range query {
		queryMap[k] = c.Query(k)
	}
	return queryMap
}

func GetHeaderParams(c *gin.Context) map[string]any {
	header := c.Request.Header
	var headerMap = make(map[string]any, len(header))
	for k, v := range header {
		if len(v) > 1 {
			headerMap[k] = v
		} else if len(v) == 1 {
			headerMap[k] = v[0]
		}
	}
	return headerMap
}

func GetPostFormParams(c *gin.Context) (map[string]any, error) {
	if err := c.Request.ParseMultipartForm(32 << 20); err != nil {
		if !errors.Is(err, http.ErrNotMultipart) {
			return nil, err
		}
	}
	var postMap = make(map[string]any, len(c.Request.PostForm))
	for k, v := range c.Request.PostForm {
		if len(v) > 1 {
			postMap[k] = v
		} else if len(v) == 1 {
			postMap[k] = v[0]
		}
	}

	return postMap, nil
}

func GetPlayload(c *gin.Context) types.H {
	bytes, _ := c.GetRawData()
	post, _ := GetPostFormParams(c)
	return types.H{
		"header": GetHeaderParams(c),
		"get":    GetQueryParams(c),
		"post":   post,
		"body":   string(bytes),
	}
}
